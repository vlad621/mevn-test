# VueJS + MongoDB + Swagger Test App

A single-page application (SPA) demonstrating the VueJS + MongoDB + Swagger stack.

## About
The frontend employs a RESTful API requesting a local MongoDB instance with Mongoose ODM on ExpressJS and NodeJS server. The [Swagger tools](https://swagger.io) were used for building and documenting the API, and the API exposes an endpoint for accessing the Swagger UI.

The source code is (c) 2020 by [Vladyslav Pavlov](mailto:vlad621pavlov@gmail.com), and is distributed by the author under the terms of [MIT License](https://opensource.org/licenses/MIT).

## Installation (Ubuntu)

1. open a Terminal window by pressing Ctrl+Alt+T. Clean apt cache and run updates.


		sudo apt-get clean
		sudo apt-get update
		sudo apt-get upgrade

2. Install Node.js.


		sudo apt-get install curl
		curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
		sudo apt-get install -y nodejs

3. Install MongoDB.


		sudo apt install -y mongodb

4. Restart MongoDB service.


		sudo service mongodb restart

5. Change to the dir where this README file is located, e.g.


		cd ~/vlad-test


6. Install prerequisite NPM modules.


		npm install


7. Import test collections to MongoDB.


		mongoimport --db vlad-test --collection clients --file ./db/export/clients.json
		mongoimport --db vlad-test --collection providers --file ./db/export/providers.json

## Running the SPA
1. Run the webapp from this README file's directory


		npm start

2. Open the client in your browser


		http://localhost:8080/

3. Open Swagger UI in another browser tab


		http://localhost:8080/api-docs/

## Stopping the SPA
Press Ctrl+C in the Terminal window.

## Stopping the MongoDB service

		sudo service mongodb stop
